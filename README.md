# Projeto 1 - Embarcados
## Como rodar

### Passo 1:
Clone o repositório em uma placa raspberry pi

```
$ git clone https://gitlab.com/murilogds/trabalho-1-embarcados.git
```

## Passo 2:
Entre na pasta do servidor central
```
$ cd central
```

## Passo 3:
Compile o código 

```
$ make
```

## Passo 4:
Execute o código informando a quantidade de servidores distribuídos que comunicarão com o servidor central (Máximo 10)

**Exemplo:**
```
$ make run 4
```

## Passo 5:
Para executar o servidor distribuído, abra um novo terminal ou acesse o código via outra raspberry Pi e entre na pasta distributed

```
$ cd distributed
```

## Passo 6:
Compile o código

```
$ make
```

## Passo 7:
Execute o código passando o nome do arquivo json usado para configurar o servidor.

**Exemplo:**

```
$ make run sala_01.json
```

**Obs.:** Caso seja necessário alterar alguma configuração (i.e. IP do servidor central, nome da sala, configuração das portas) é necessário atualizar o arquivo JSON da sala ou criar um novo com base nos existentes e passar esse arquivo no comando.

## Link da apresentação
https://youtu.be/rB06ogm-OUc
