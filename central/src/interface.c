#include <interface.h>

void defineFireAlarmSistem()
{
    int fireAlarmStatus = getStatusFireAlarm();
    printf("----------------------\n");
    if (fireAlarmStatus == 1)
        printf("Desativar sistema de alarme de incendio? [1=Sim\\2=Nao]\n");
    else
        printf("Ativar sistema de alarme de incendio? [1=Sim\\2=Nao]\n");
    int opt;
    scanf("%d", &opt);
    switch (opt)
    {
    case 1:
        if (toggleFireAlarmStatus() == -1)
        {
            printf("Impossivel ativar sistema, verifique se todos os sensores estao desativados\n");
            printf("Digite 1 para voltar a pagina anterior\n");
            scanf("%d", &opt);
            break;
        }
    default:
        break;
    }
}

void defineAlarmSistem()
{
    int alarmStatus = getStatusAlarm();
    printf("----------------------\n");
    if (alarmStatus == 1)
        printf("Desativar sistema de alarme? [1=Sim\\2=Nao]\n");
    else
        printf("Ativar sistema de alarme? [1=Sim\\2=Nao]\n");
    int opt;
    scanf("%d", &opt);
    switch (opt)
    {
    case 1:
        if (toggleAlarmStatus() == -1)
        {
            printf("Impossivel ativar sistema, verifique se todos os sensores estao desativados\n");
            printf("Digite 1 para voltar a pagina anterior\n");
            scanf("%*d");
            break;
        }
    default:
        break;
    }
}

int getTotalCount(gpioStatus status)
{
    int sum = 0;
    for (int i = 0; i < getDevicesNumber(); i++)
    {
        sum += status.devices[i].contPessoas;
    }
    return sum;
}

void printHeader()
{
    system("clear");
    printf("------------ CONTROLE PREDIAL ---------------\n");
}

void printDeviceStatus(int device)
{
    int opt = 0;
    while(opt != 2)
    {
        gpioStatus currentStatus = getCurrentStatus();
        printHeader();
        printf("%s\n", getDevice(device - 1));
        printf("---------------------------------------------\n");
        printf("SENSORES:\n");
        printf("SENSOR DE PRESENCA: %s\n", currentStatus.devices[device - 1].SPres ? "Ativo" : "Inativo");
        printf("SENSOR DE FUMACA: %s\n", currentStatus.devices[device - 1].SFum ? "Ativo" : "Inativo");
        printf("SENSOR DA PORTA: %s\n", currentStatus.devices[device - 1].SPor ? "Ativo" : "Inativo");
        printf("SENSOR DA JANELA: %s\n", currentStatus.devices[device - 1].SJan ? "Ativo" : "Inativo");
        printf("----------------------------------------------\n");
        printf("CONTAGEM DE PESSOAS: %d\n", currentStatus.devices[device - 1].contPessoas);
        printf("----------------------------------------------\n");
        printf("TEMPERATURA E UMIDADE: \n");
        printf("Temperatura: %.2f *C\n", currentStatus.devices[device - 1].temp);
        printf("Umidade: %.2f\n", currentStatus.devices[device - 1].humid);
        printf("---------------------------------------------\n");
        printf("SAIDAS: \n");
        printf("LAMPADA 1: %s\n", currentStatus.devices[device - 1].L_01 ? "Ligada" : "Desligada");
        printf("LAMPADA 2: %s\n", currentStatus.devices[device - 1].L_02 ? "Ligada" : "Desligada");
        printf("PROJETOR: %s\n", currentStatus.devices[device - 1].PR ? "Ligado" : "Desligado");
        printf("AR-CONDICIONADO: %s\n", currentStatus.devices[device - 1].AC ? "Ligado" : "Desligado");
        printf("ALARME: %s\n", currentStatus.devices[device - 1].AL_BZ ? "Ligado" : "Desligado");
        printf("----------------------------------------------\n");
        printf("1 - Atualizar\n");
        printf("2 - Voltar\n");
        scanf("%d", &opt);
    }
}

void setDeviceStatus()
{
    char buffer[MAX];
    char *disp;
    char *action;
    int opt = 0;
    int devices = getDevicesNumber();
    while(1)
    {
        memset(buffer, 0x0, MAX);
        printHeader();
        for(int i = 0; i < devices; i++)
        {
            printf("%d - %s\n", i+1, getDevice(i));
        }
        printf("%d - Sistema de alarme\n", devices + 1);
        printf("%d - Sistema de alarme de incendio\n", devices+2);
        printf("%d - Desligar todas as cargas do predio\n", devices + 3);
        printf("%d - Voltar\n", devices+4);
        int device;
        scanf("%d", &device);
        if (device == devices + 1) defineAlarmSistem();
        else if (device == devices + 2) defineFireAlarmSistem();
        else if (device == devices + 3) turnOffAllOutputs();
        else if (device == devices + 4) break;
        if (device > devices) continue;
        sprintf(buffer, "%d ", device-1);
        printHeader();
        printf("1 - Lâmpada 01\n");
        printf("2 - Lâmpada 02\n");
        printf("3 - Projetor\n");
        printf("4 - Ar condicionado\n");
        printf("5 - Alarme\n");
        printf("6 - Ligar todas as lampadas da sala\n");
        printf("7 - Desligar todas as cargas da sala\n");
        scanf("%d", &opt);
        switch (opt)
        {
        case 1:
            disp = "L_01";
            break;
        case 2:
            disp = "L_02";
            break;
        case 3:
            disp = "PR";
            break;
        case 4:
            disp = "AC";
            break;
        case 5:
            disp = "AL_BZ";
            break;
        case 6:
            turnOnRoomLights(device - 1);
            break;
        case 7:
            turnOffRoomOutputs(device - 1);
            break;
        default:
            break;
        }
        if (opt > 5)
            continue;
        printf("------------\n");
        printf("1 - Ligar\n");
        printf("2 - Desligar\n");
        scanf("%d", &opt);
        switch (opt)
        {
        case 1:
            action = " UP";
            break;
        case 2:
            action = " DOWN";
            break;
        default:
            break;
        }
        strcat(buffer, disp);
        strcat(buffer, action);
        if (strcmp(buffer, "") != 0)
        {
            setSendMessage(buffer);
        }
    }
}

void *interface(void *arg)
{
    int opt;
    while (1)
    {
        
        printHeader();
        printf("1 - Monitoramento\n");
        printf("2 - Controle\n");
        printf("3 - Sair\n");
        scanf("%d", &opt);
        if (opt == 1)
        {
            opt = 0;
            while(1)
            {
                printHeader();
                printf("CONTAGEM TOTAL DE PESSOAS: %d\n", getTotalCount(getCurrentStatus()));
                printf("---------------------------------------------\n");
                int devices = getDevicesNumber();
                int id;
                for(int i = 0; i < devices; i++)
                {
                    printf("%d - %s\n", i+1, getDevice(i));
                }
                printf("%d - Voltar\n", devices + 1);
                printf("---------------------------------------------\n");
                scanf("%d", &id);
                if (id > devices) break;
                printDeviceStatus(id);
            }
            continue;
        }
        if (opt == 2)
        {
            setDeviceStatus();
        }
        if (opt == 3)
        {
            exit(0);
        }
    }
}