#include <server.h>

char sendMessage[MAX], serverMessage[MAX];
char devices[MAX_SOCKETS][MAX];
sem_t semSendMessage, semServerMessage, semDevices;
int devicesNumber;

typedef struct deviceConnection
{
    int index;
    int socketId;
} deviceConnection;

void setSendMessage(char *message)
{
    while (1)
    {
        sem_wait(&semSendMessage);
        if (strcmp(sendMessage, "") == 0)
        {
            strcpy(sendMessage, message);
            sem_post(&semSendMessage);
            break;
        }
        else
        {
            sem_post(&semSendMessage);
        }
        usleep(1000);
    }
}

void setServerMessage(char *message, int index)
{
    while (1)
    {
        sem_wait(&semServerMessage);
        if (strcmp(serverMessage, "") == 0)
        {
            sprintf(serverMessage, "%d %s", index, message);
            sem_post(&semServerMessage);
            break;
        }
        else
        {
            sem_post(&semServerMessage);
        }
        usleep(1000);
    }
}

void receive_from_server(int socket_id, int index)
{
    char buff[MAX];
    int firstMessage = 1;
    // infinite loop for chat
    for (;;)
    {
        memset(buff, 0x0, MAX);

        // read the message from client and copy it in buffer
        if (recv(socket_id, buff, MAX, 0) < 0)
        {
            perror("recv");
            exit(1);
        }
        if (firstMessage)
        {
            strcpy(devices[index], buff);
            firstMessage = 0;
        }
        else if (strcmp(buff, "") != 0)
        {
            setServerMessage(buff, index);
        }

        sem_wait(&semSendMessage);
        char *token;
        int shouldSend = 1;
        memset(buff, 0x0, MAX);
        strcpy(buff, sendMessage);
        if (strchr(sendMessage, ' ') != NULL)
        {
            token = strtok(buff, " ");
            if (strchr(token, index + '0') != NULL) shouldSend = 1;
            else shouldSend = 0;
            token = strtok(NULL, " ");
            strcpy(buff, token);
            token = strtok(NULL, " ");
            strcat(buff, " ");
            strcat(buff, token);
        }
        if (!shouldSend)
        {
            strcpy(buff, "");
        }
        else
        {
            memset(sendMessage, 0x0, MAX);
            strcpy(sendMessage, "");
        }
        if (send(socket_id, buff, MAX, 0) < 0)
        {
            exit(1);
        }
        sem_post(&semSendMessage);

        // if msg contains "Exit" then server exit and chat ended.
        if (strncmp("exit", buff, 4) == 0)
        {
            break;
        }
        usleep(200);
    }
}

void *connectSocket(void *arg)
{
    struct sockaddr_in client;
    deviceConnection *connectionPtr = (deviceConnection *)arg;
    deviceConnection connection = *connectionPtr;
    unsigned int len = sizeof(client);
    int connectionId = accept(connection.socketId, (struct sockaddr *)&client, &len);
    if (connectionId < 0)
    {
        perror("Accept");
        exit(0);
    }
    receive_from_server(connectionId, connection.index);

    close(connection.socketId);
}

void *startServer(void  *arg)
{
    int *serverNumberPtr = (int *)arg;
    int serverNumber = *serverNumberPtr;
    devicesNumber = serverNumber;
    int socketId = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = PF_INET;
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddress.sin_port = 10322;

    // Binding newly created socket to given IP and verification
    if ((bind(socketId, (struct sockaddr *)&serverAddress, sizeof(serverAddress))) != 0)
    {
        perror("socket bind failed...\n");
        exit(0);
    }

    if (listen(socketId, 10) < 0)
    {
        perror("Listen");
        exit(0);
    }

    pthread_t socketThreads[MAX_SOCKETS];
    deviceConnection connections[MAX_SOCKETS];
    for (int i=0; i<serverNumber; i++)
    {
        connections[i].socketId = socketId;
        connections[i].index = i;
        pthread_create(&socketThreads[i], NULL, &connectSocket, (void *)&connections[i]);
    }

    while(1)
    {
        sleep(1);
    }

    return NULL;
}

void getCurrentServerMessage(char **message)
{
    sem_wait(&semServerMessage);
    *message = malloc(strlen(serverMessage) + 1);
    strcpy(*message, serverMessage);
    strcpy(serverMessage, "");
    sem_post(&semServerMessage);
}

void initServerSemaphores()
{
    sem_init(&semSendMessage, 0, 1);
    sem_init(&semServerMessage, 0, 1);
}

int getDevicesNumber()
{
    return devicesNumber;
}

char *getDevice(int index)
{
    return devices[index];
}