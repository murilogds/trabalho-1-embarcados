#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <interface.h>
#include <gpioStatus.h>
#include <server.h>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        printf("Eh necessario passar o numero de servidores distribuidos\n");
        return 1;
    }
    int serverNumber = atoi(argv[1]);
    initServerSemaphores();
    initGPIOStatus(serverNumber);
    pthread_t serverThread, interfaceThread, handleMessageThread;
    pthread_create(&serverThread, NULL, startServer, (void *)&serverNumber);
    pthread_create(&interfaceThread, NULL, interface, NULL);
    pthread_create(&handleMessageThread, NULL, handleMessage, NULL);

    while (1)
    {
        sleep(1);
    }

    return 0;
}