#include <gpioStatus.h>

gpioStatus status;
sem_t semStatus;

void *turnLightsOn(void *arg)
{
    int *idPtr = (int *)arg;
    int id = *idPtr;
    char sendMessage[MAX];
    sprintf(sendMessage, "%d L_01 UP", id);
    setSendMessage(sendMessage);
    sprintf(sendMessage, "%d L_02 UP", id);
    setSendMessage(sendMessage);
    sleep(15);
    sprintf(sendMessage, "%d L_01 DOWN", id);
    setSendMessage(sendMessage);
    sprintf(sendMessage, "%d L_02 DOWN", id);
    setSendMessage(sendMessage);
    return NULL;
}

void fireAlarm()
{
    char sendMessage[MAX];
    for(int i = 0; i < getDevicesNumber(); i++)
    {
        sprintf(sendMessage, "%d AL_BZ UP", i);
        setSendMessage(sendMessage);
    }
}

void handlePresenceSensor(int *id)
{
    if (status.isAlarmActive)
    {
        fireAlarm();
    }
    else 
    {
        pthread_t turnLightsOnThread;
        pthread_create(&turnLightsOnThread, NULL, turnLightsOn, (void *)id);
    }
}

void *handleMessage(void *arg)
{
    char *message = "";
    int id = 0;
    char *token;
    while(1)
    {
        getCurrentServerMessage(&message);
        if (strcmp(message, "") == 0)
        {
            free(message);
            usleep(50000);
            continue;
        }
        token = strtok(message, " ");
        id = atoi(token);
        strcpy(message, "");
        unsigned short cont = 0;
        while((token = strtok(NULL, " ")) != NULL)
        {
            if (cont > 0) strcat(message, " ");
            strcat(message, token);
            cont++;
        }
        sem_wait(&semStatus);
        if (strcmp(message, "SPres") == 0)
        {
            status.devices[id].SPres = (status.devices[id].SPres + 1)%2;
            if (status.devices[id].SPres)
            {
                handlePresenceSensor(&id);
            }
        }
        else if(strcmp(message, "SFum") == 0)
        {
            status.devices[id].SFum = (status.devices[id].SFum + 1)%2;
            if (status.isFireAlarmActive)
            {
                fireAlarm();
            }
        }
        else if(strcmp(message, "SPor") == 0)
        {
            status.devices[id].SPor = (status.devices[id].SPor + 1)%2;
            if (status.devices[id].SPor)
            {
                handlePresenceSensor(&id);
            }
        }
        else if(strcmp(message, "SJan") == 0)
        {
            status.devices[id].SJan = (status.devices[id].SJan + 1)%2;
            if (status.devices[id].SJan)
            {
                handlePresenceSensor(&id);
            }
        }
        else if(strncmp(message, "CONT", 4) == 0)
        {
            char cont[MAX];
            token = strtok(message, " ");
            token = strtok(NULL, " ");
            strcpy(cont, token);
            status.devices[id].contPessoas = atoi(cont);
        }
        else if(strncmp(message, "TEMP", 4) == 0)
        {
            char cont[MAX];
            token = strtok(message, " ");
            token = strtok(NULL, " ");
            strcpy(cont, token);
            status.devices[id].temp = atof(cont);
        }
        else if(strncmp(message, "HUM", 3) == 0)
        {
            char cont[MAX];
            token = strtok(message, " ");
            token = strtok(NULL, " ");
            strcpy(cont, token);
            status.devices[id].humid = atof(cont);
        }
        else if(strcmp(message, "L_01") == 0)
        {
            status.devices[id].L_01 = (status.devices[id].L_01 + 1)%2;
        }
        else if(strcmp(message, "L_02") == 0)
        {
            status.devices[id].L_02 = (status.devices[id].L_02 + 1)%2;
        }
        else if(strcmp(message, "AC") == 0)
        {
            status.devices[id].AC = (status.devices[id].AC + 1)%2;
        }
        else if(strcmp(message, "PR") == 0)
        {
            status.devices[id].PR = (status.devices[id].PR + 1)%2;
        }
        else if(strcmp(message, "AL_BZ") == 0)
        {
            status.devices[id].AL_BZ = (status.devices[id].AL_BZ + 1)%2;
        }
        sem_post(&semStatus);
        usleep(50000);
        free(message);
    }
}

void initGPIOStatus(int devices)
{
    sem_init(&semStatus, 0, 1);
    status.isAlarmActive = 0;
    status.isFireAlarmActive = 0;
    for (int i = 0; i < devices; i++)
    {
        status.devices[i].SPres = 0;
        status.devices[i].SFum = 0;
        status.devices[i].SJan = 0;
        status.devices[i].SPor = 0;
        status.devices[i].contPessoas = 0;
        status.devices[i].L_01 = 0;
        status.devices[i].L_02 = 0;
        status.devices[i].AC = 0;
        status.devices[i].PR = 0;
    }
}

int getStatusFireAlarm()
{
    sem_wait(&semStatus);
    int fireAlarm = status.isFireAlarmActive;
    sem_post(&semStatus);
    return fireAlarm;
}

int getStatusAlarm()
{
    sem_wait(&semStatus);
    int alarm = status.isAlarmActive;
    sem_post(&semStatus);
    return alarm;
}

int toggleAlarmStatus()
{
    sem_wait(&semStatus);
    int shouldActiveAlarm = (status.isAlarmActive + 1) % 2;
    if (shouldActiveAlarm)
    {
        for (int i = 0; i < getDevicesNumber(); i++)
        {
            if (status.devices[i].SPres || status.devices[i].SPor || status.devices[i].SJan)
            {
                sem_post(&semStatus);
                return -1;
            }
        }
    }
    status.isAlarmActive = shouldActiveAlarm;
    sem_post(&semStatus);
    return 0;
}

int toggleFireAlarmStatus()
{
    sem_wait(&semStatus);
    int shouldActiveAlarm = (status.isFireAlarmActive + 1) % 2;
    if (shouldActiveAlarm)
    {
        for (int i = 0; i < getDevicesNumber(); i++)
        {
            if (status.devices[i].SFum)
            {
                sem_post(&semStatus);
                return -1;
            }
        }
    }
    status.isFireAlarmActive = shouldActiveAlarm;
    sem_post(&semStatus);
    return 0;
}

gpioStatus getCurrentStatus()
{
    sem_wait(&semStatus);
    gpioStatus currentStatus = status;
    sem_post(&semStatus);
    return currentStatus;
}

void turnOnRoomLights(int roomId)
{
    char message[MAX];
    sprintf(message,"%d L_01 UP", roomId);
    setSendMessage(message);
    sprintf(message,"%d L_02 UP", roomId);
    setSendMessage(message);
}

void turnOffRoomOutputs(int roomId)
{
    char message[MAX];
    sprintf(message,"%d PR DOWN", roomId);
    setSendMessage(message);
    sprintf(message,"%d AC DOWN", roomId);
    setSendMessage(message);
    sprintf(message,"%d L_02 DOWN", roomId);
    setSendMessage(message);
    sprintf(message,"%d L_01 DOWN", roomId);
    setSendMessage(message);
}

void turnOffAllOutputs()
{
    char message[MAX];
    for(int i = 0; i < getDevicesNumber(); i++)
    {
        sprintf(message,"%d PR DOWN", i);
        setSendMessage(message);
        sprintf(message,"%d AC DOWN", i);
        setSendMessage(message);
        sprintf(message,"%d L_02 DOWN", i);
        setSendMessage(message);
        sprintf(message,"%d L_01 DOWN", i);
        setSendMessage(message);
    }
}