#ifndef INTERFACE_H
#define INTERFACE_H

#include <stdio.h>
#include <gpioStatus.h>
#include <server.h>

void *interface(void *arg);

#endif