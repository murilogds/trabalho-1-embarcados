#ifndef SERVER_H
#define SERVER_H

#include <semaphore.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>

#define MAX 15
#define MAX_SOCKETS 5

void setSendMessage(char *message);

void *startServer(void *arg);

void getCurrentServerMessage(char **message);

void initServerSemaphores();

int getDevicesNumber();

char *getDevice(int index);

#endif