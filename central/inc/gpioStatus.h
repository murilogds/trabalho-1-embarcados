#ifndef GPIO_STATUS_H
#define GPIO_STATUS_H

#include <semaphore.h>
#include <stdio.h>
#include <server.h>
#include <stdlib.h>
#include <pthread.h>

typedef struct deviceStatus
{
    float temp;
    float humid;
    int L_01;
    int L_02;
    int PR;
    int AC;
    int AL_BZ;
    int SPres;
    int SFum;
    int SJan;
    int SPor;
    int contPessoas;
} deviceStatus;

typedef struct gpioStatus
{
    int isAlarmActive;
    int isFireAlarmActive;
    deviceStatus devices[MAX_SOCKETS];
} gpioStatus;

void initGPIOStatus(int devices);

void *handleMessage(void *arg);

int getStatusFireAlarm();

int getStatusAlarm();

int toggleAlarmStatus();

int toggleFireAlarmStatus();

void turnOnRoomLights(int roomId);

void turnOffRoomOutputs(int roomId);

void turnOffAllOutputs();

gpioStatus getCurrentStatus();

#endif