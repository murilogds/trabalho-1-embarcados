#include <bcm2835.h>
#include <stdio.h>
#include <string.h>
#include <sched.h>
#include <unistd.h>
#include <stdbool.h>
#include <gpio.h>

#define MAXTIMINGS 100

void *readDHTLoop(void *arg);