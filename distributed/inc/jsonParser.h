#ifndef JSONPARSER_H
#define JSONPARSER_H

#include <cJSON.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_FILE_SIZE 10000

cJSON* getJsonFromFile(char *path);
int getOutputGPIO(cJSON* json, char* tag);
int getInputGPIO(cJSON* json, char* tag);
int getSensorTempGPIO(cJSON* json, char* tag);
char *getCentralServerIP(cJSON* json);
unsigned int getCentralServerPort(cJSON* json);
char *getName(cJSON* json);

#endif