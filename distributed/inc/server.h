#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <jsonParser.h>
#include <unistd.h>
#include <semaphore.h>

#define MAX_MSG 15

typedef struct connectionArgs {
    char* ip;
    unsigned int port;
    char* roomName;
} connectionArgs;

void *startConnection(void *args);
void getCurrentServerMessage(char **message);
void setupServerSemaphores();
void setSendMessage(char *message);
connectionArgs getConnectionFromArgsFromJson(cJSON *json);

#endif