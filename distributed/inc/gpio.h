#ifndef GPIO_H
#define GPIO_H

#include <jsonParser.h>
#include <bcm2835.h>
#include <signal.h>
#include <unistd.h>
#include <dht22.h>
#include <server.h>

typedef struct gpio {
    int L_01;
    int L_02;
    int AC;
    int PR;
    int AL_BZ;
    int SPres;
    int SFum;
    int SJan;
    int SPor;
    int SC_IN;
    int SC_OUT;
    int DHT22;
} gpio;

gpio configureGPIOFromJson(cJSON* json);
void up(int pin);
void down(int pin);
void setupGPIO(gpio gpio);
void *watchInputs(void *arg);
void killGpio(int sig);

#endif