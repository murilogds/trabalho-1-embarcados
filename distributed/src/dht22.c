
#include <dht22.h>

void readDHT(int pin, float *humid0, float *temp0)
{
    int counter = 0;
    int laststate = HIGH;
    int j = 0;

    int data[100];

    // Set GPIO pin to output
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);

    bcm2835_gpio_write(pin, HIGH);
    usleep(500000); // 500 ms
    bcm2835_gpio_write(pin, LOW);
    usleep(20000);

    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_INPT);

    data[0] = data[1] = data[2] = data[3] = data[4] = 0;

    // wait for pin to drop?
    while (bcm2835_gpio_lev(pin) == 1)
    {
        usleep(1);
    }

    // read data!
    for (int i = 0; i < MAXTIMINGS; i++)
    {
        counter = 0;
        while (bcm2835_gpio_lev(pin) == laststate)
        {
            counter++;
            // nanosleep(1);		// overclocking might change this?
            if (counter == 1000)
                break;
        }

        laststate = bcm2835_gpio_lev(pin);
        if (counter == 1000)
            break;

        if ((i > 3) && (i % 2 == 0))
        {
            // shove each bit into the storage bytes
            data[j / 8] <<= 1;
            if (counter > 200)
                data[j / 8] |= 1;
            j++;
        }
    }

    if ((j >= 39) && (data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)))
    { // yay!
        float f, h;
        h = data[0] * 256 + data[1];
        h /= 10;

        f = (data[2] & 0x7F) * 256 + data[3];
        f /= 10.0;
        if (data[2] & 0x80)
        {
            f *= -1;
        }

        *humid0 = h;
        *temp0 = f;
    }
}

void *readDHTLoop(void *arg)
{
    signal(SIGINT, killGpio);
    int *pinPtr = (int *)arg;
    int pin = *pinPtr;
    float humid = 0.0f, temp = 0.0f;
    char tempMessage[MAX_MSG], humMessage[MAX_MSG];
    while(1) {
        readDHT(pin, &humid, &temp);
        if (temp != 0 && humid != 0)
        {
            sprintf(tempMessage, "TEMP %.1f", temp);
            sprintf(humMessage, "HUM %.1f", humid);
            setSendMessage(tempMessage);
            setSendMessage(humMessage);
            // sendDHTMessage(humid, temp)
        }
        delay(1500);
    }
}