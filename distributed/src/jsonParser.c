#include <jsonParser.h>

cJSON *getJsonFromFile(char *path)
{
    FILE *ptr;
    char jsonString[MAX_FILE_SIZE];
    memset(jsonString, 0x0, MAX_FILE_SIZE);

    // Opening file in reading mode
    ptr = fopen(path, "r");

    if (NULL == ptr)
    {
        printf("file can't be opened \n");
        exit(1);
    }

    fread(&jsonString[0], sizeof(char), MAX_FILE_SIZE, ptr);

    fclose(ptr);
    cJSON *json = cJSON_Parse(jsonString);
    return json;
}

int getOutputGPIO(cJSON *json, char *tag)
{
    const cJSON *outputs = NULL;
    const cJSON *output = NULL;
    outputs = cJSON_GetObjectItemCaseSensitive(json, "outputs");
    cJSON_ArrayForEach(output, outputs)
    {
        cJSON *tagJson = cJSON_GetObjectItemCaseSensitive(output, "tag");
        if (cJSON_IsString(tagJson) && strcmp(tagJson->valuestring, tag) == 0)
        {
            cJSON *gpio = cJSON_GetObjectItemCaseSensitive(output, "gpio");

            if (cJSON_IsNumber(gpio))
            {
                return gpio->valueint;
            }
        }
    }
    printf("Error reading gpio %s from JSON\n", tag);
    exit(1);
}

int getInputGPIO(cJSON *json, char *tag)
{
    const cJSON *inputs = NULL;
    const cJSON *input = NULL;
    inputs = cJSON_GetObjectItemCaseSensitive(json, "inputs");
    cJSON_ArrayForEach(input, inputs)
    {
        cJSON *tagJson = cJSON_GetObjectItemCaseSensitive(input, "tag");
        if (cJSON_IsString(tagJson) && strcmp(tagJson->valuestring, tag) == 0)
        {
            cJSON *gpio = cJSON_GetObjectItemCaseSensitive(input, "gpio");

            if (cJSON_IsNumber(gpio))
            {
                return gpio->valueint;
            }
        }
    }
    printf("Error reading gpio %s from JSON\n", tag);
    exit(1);
}

int getSensorTempGPIO(cJSON *json, char *tag)
{
    const cJSON *sensors = NULL;
    const cJSON *sensor = NULL;
    sensors = cJSON_GetObjectItemCaseSensitive(json, "sensor_temperatura");
    cJSON_ArrayForEach(sensor, sensors)
    {
        cJSON *tagJson = cJSON_GetObjectItemCaseSensitive(sensor, "tag");
        if (cJSON_IsString(tagJson) && strcmp(tagJson->valuestring, tag) == 0)
        {
            cJSON *gpio = cJSON_GetObjectItemCaseSensitive(sensor, "gpio");

            if (cJSON_IsNumber(gpio))
            {
                return gpio->valueint;
            }
        }
    }
    printf("Error reading gpio %s from JSON\n", tag);
    exit(1);
}

char *getCentralServerIP(cJSON *json)
{
    const cJSON *ipJSON = cJSON_GetObjectItemCaseSensitive(json, "ip_servidor_central");
    if (cJSON_IsString(ipJSON)) return ipJSON->valuestring;
    printf("Error reading central server ip from JSON\n");
    exit(1);
}

unsigned int getCentralServerPort(cJSON *json)
{
    const cJSON *ipJSON = cJSON_GetObjectItemCaseSensitive(json, "porta_servidor_central");
    if (cJSON_IsNumber(ipJSON)) return ipJSON->valueint;
    printf("Error reading central server port from JSON\n");
    exit(1);
}

char *getName(cJSON* json)
{
    const cJSON *name = cJSON_GetObjectItemCaseSensitive(json, "nome");
    if (cJSON_IsString(name)) return name->valuestring;
    printf("Error reading device name from JSON\n");
    exit(1);
}