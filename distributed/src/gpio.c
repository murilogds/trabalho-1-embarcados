#include <gpio.h>

gpio configureGPIOFromJson(cJSON* json) {
    gpio gpio;
    gpio.L_01 = getOutputGPIO(json, "Lâmpada 01");
    gpio.L_02 = getOutputGPIO(json, "Lâmpada 02");
    gpio.AC = getOutputGPIO(json, "Ar-Condicionado (1º Andar)");
    gpio.PR = getOutputGPIO(json, "Projetor Multimidia");
    gpio.AL_BZ = getOutputGPIO(json, "Sirene do Alarme");
    gpio.SPres = getInputGPIO(json, "Sensor de Presença");
    gpio.SFum = getInputGPIO(json, "Sensor de Fumaça");
    gpio.SJan = getInputGPIO(json, "Sensor de Janela");
    gpio.SPor = getInputGPIO(json, "Sensor de Porta");
    gpio.SC_IN = getInputGPIO(json, "Sensor de Contagem de Pessoas Entrada");
    gpio.SC_OUT = getInputGPIO(json, "Sensor de Contagem de Pessoas Saída");
    gpio.DHT22 = getSensorTempGPIO(json, "Sensor de Temperatura e Umidade");
    return gpio;
}

void up(int pin) {
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(pin, 1);
    delay(50);
}

void down(int pin) {
    bcm2835_gpio_fsel(pin, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_write(pin, 0);
    delay(50);
}

void setupGPIO(gpio gpio) {
    if (!bcm2835_init())
        exit(1);

    bcm2835_gpio_fsel(gpio.L_01, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(gpio.L_02, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(gpio.AC, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(gpio.PR, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(gpio.AL_BZ, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(gpio.SPres, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(gpio.SFum, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(gpio.SJan, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(gpio.SPor, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(gpio.SC_IN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(gpio.SC_OUT, BCM2835_GPIO_FSEL_INPT);
}

void killGpio(int signal) {
    bcm2835_close();
    exit(1);
}

void handleServerMessage(gpio gpio)
{
    char *message;
    getCurrentServerMessage(&message);
    if (strcmp(message, "") == 0)
        return;
    printf("Received message: %s\n", message);
    char tempMessage[MAX_MSG], id[MAX_MSG], action[MAX_MSG];
    strcpy(tempMessage, message);
    char *token = strtok(message, " ");
    strcpy(id, token);
    token = strtok(NULL, " ");
    strcpy(action, token);
    int pinId = 0;
    if (strcmp("L_01", id) == 0) pinId = gpio.L_01;
    if (strcmp("L_02", id) == 0) pinId = gpio.L_02;
    if (strcmp("AC", id) == 0) pinId = gpio.AC;
    if (strcmp("PR", id) == 0) pinId = gpio.PR;
    if (strcmp("AL_BZ", id) == 0) pinId = gpio.AL_BZ;

    if (pinId != 0) {
        if (strcmp("DOWN", action) == 0) down(pinId);
        if (strcmp("UP", action) == 0) up(pinId);
    }
    free(message);
}

void *watchInputs(void *arg) {
    gpio *gpioPtr = (gpio*) arg;
    gpio gpio = *gpioPtr;
    signal(SIGINT, killGpio);
    unsigned int contPessoas = 0, previous = 1, cont = 0;
    unsigned int currentSPres = 0, currentSFum = 0, currentSJan = 0, currentSPor = 0;
    unsigned int currentL_01 = 0, currentL_02 = 0, currentAC = 0, currentPR = 0, currentAL_BZ = 0;

    char msgContPessoas[MAX_MSG];
    while(1) {
        handleServerMessage(gpio);
        if (cont%5==0) {
            if (currentL_01 != bcm2835_gpio_lev(gpio.L_01))
            {
                setSendMessage("L_01");
                currentL_01 = bcm2835_gpio_lev(gpio.L_01);
            }
            if (currentL_02 != bcm2835_gpio_lev(gpio.L_02))
            {
                setSendMessage("L_02");
                currentL_02 = bcm2835_gpio_lev(gpio.L_02);
            }
            if (currentAC != bcm2835_gpio_lev(gpio.AC))
            {
                setSendMessage("AC");
                currentAC = bcm2835_gpio_lev(gpio.AC);
            }
            if (currentPR != bcm2835_gpio_lev(gpio.PR))
            {
                setSendMessage("PR");
                currentPR = bcm2835_gpio_lev(gpio.PR);
            }
            if (currentAL_BZ != bcm2835_gpio_lev(gpio.AL_BZ))
            {
                setSendMessage("AL_BZ");
                currentAL_BZ = bcm2835_gpio_lev(gpio.AL_BZ);
            }
            if (currentSPres != bcm2835_gpio_lev(gpio.SPres))
            {
                setSendMessage("SPres");
                currentSPres = bcm2835_gpio_lev(gpio.SPres);
            }
            if (currentSFum != bcm2835_gpio_lev(gpio.SFum))
            {
                setSendMessage("SFum");
                currentSFum = bcm2835_gpio_lev(gpio.SFum);
            }
            if (currentSJan != bcm2835_gpio_lev(gpio.SJan))
            {
                setSendMessage("SJan");
                currentSJan = bcm2835_gpio_lev(gpio.SJan);
            }
            if (currentSPor != bcm2835_gpio_lev(gpio.SPor))
            {
                setSendMessage("SPor");
                currentSPor = bcm2835_gpio_lev(gpio.SPor);
            }
        }
        if (1 == bcm2835_gpio_lev(gpio.SC_IN)) contPessoas++;
        if (1 == bcm2835_gpio_lev(gpio.SC_OUT)) {
            contPessoas = contPessoas == 0
                ? 0
                : contPessoas - 1;
        }
        if (previous != contPessoas)
        {
            sprintf(msgContPessoas, "CONT %d", contPessoas);
            setSendMessage(msgContPessoas);
        }
        previous = contPessoas;
        cont++;
        cont %= 10;
        delay(200);
    }
}