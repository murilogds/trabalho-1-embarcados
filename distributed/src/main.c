#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <stdlib.h>
#include <pthread.h>
#include <jsonParser.h>
#include <gpio.h>
#include <dht22.h>
#include <server.h>

int main(int argc, char* argv[])
{
    if (argc < 2) {
        printf("Eh necessario passar o caminho para o arquivo json!\n");
        return -1;
    }
    cJSON *json = getJsonFromFile(argv[1]);
    gpio gpio = configureGPIOFromJson(json);
    connectionArgs connectionArgs = getConnectionFromArgsFromJson(json);
    setupGPIO(gpio);
    setupServerSemaphores();
    pthread_t watchInputsThread, dht22Thread, socketThread;
    pthread_create(&socketThread, NULL, startConnection, (void *)&connectionArgs);
    pthread_create(&watchInputsThread, NULL, watchInputs, (void *)&gpio);
    pthread_create(&dht22Thread, NULL, readDHTLoop, (void *)&gpio.DHT22);

    while (1)
    {
        sleep(1);
    }
    
    return 0;
}