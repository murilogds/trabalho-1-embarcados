#include <server.h>

char serverMessage[MAX_MSG];
char sendMessage[MAX_MSG];
sem_t semServerMessage, semSendMessage;

void setServerMessage(char *message)
{
    while (1)
    {
        sem_wait(&semServerMessage);
        if (strcmp(serverMessage, "") == 0)
        {
            strcpy(serverMessage, message);
            sem_post(&semServerMessage);
            break;
        }
        else
        {
            sem_post(&semServerMessage);
        }
    }
}

void handleMessages(int connection)
{
    char buff[MAX_MSG];
    for (;;)
    {
        sem_wait(&semSendMessage);
        if (send(connection, sendMessage, MAX_MSG, 0) < 0)
        {
            perror("Send");
            exit(1);
        }
        strcpy(sendMessage, "");
        sem_post(&semSendMessage);

        memset(buff, 0x0, MAX_MSG);
        if (recv(connection, buff, MAX_MSG, 0) < 0)
        {
            perror("recv");
            exit(1);
        }
        if (strcmp(buff, "") != 0)
        {
            setServerMessage(buff);
        }
        if ((strncmp(buff, "exit", 4)) == 0)
        {
            printf("Client Exit...\n");
            break;
        }
    }
}

void *startConnection(void *args)
{
    printf("Try to connect\n");
    connectionArgs *connectionArgsPtr = (connectionArgs *)args;
    connectionArgs connectionArgs = *connectionArgsPtr;
    setSendMessage(connectionArgs.roomName);
    int socketId = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    struct sockaddr_in serverAddress;
    serverAddress.sin_family = PF_INET;
    serverAddress.sin_addr.s_addr = inet_addr(connectionArgs.ip);
    serverAddress.sin_port = connectionArgs.port;

    if (connect(socketId, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        perror("Connect");
        exit(1);
    }
    printf("Connected to socket\n");
    handleMessages(socketId);

    return NULL;
}

void setSendMessage(char *message)
{
    while (1)
    {
        sem_wait(&semSendMessage);
        if (strcmp(sendMessage, "") == 0)
        {
            strcpy(sendMessage, message);
            sem_post(&semSendMessage);
            break;
        }
        else
        {
            sem_post(&semSendMessage);
        }
    }
}

void getCurrentServerMessage(char **message)
{
    sem_wait(&semServerMessage);
    *message = malloc(strlen(serverMessage) + 1);
    strcpy(*message, serverMessage);
    strcpy(serverMessage, "");
    sem_post(&semServerMessage);
}

void setupServerSemaphores()
{
    strcpy(sendMessage, "");
    sem_init(&semServerMessage, 0, 1);
    sem_init(&semSendMessage, 0, 1);
}

connectionArgs getConnectionFromArgsFromJson(cJSON *json)
{
    connectionArgs args;
    args.ip = getCentralServerIP(json);
    args.port = getCentralServerPort(json);
    args.roomName = getName(json);
    return args;
}
